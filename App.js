import React, { PureComponent } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { RNCamera } from 'react-native-camera';
import { Dimensions } from "react-native";
import { accelerometer, setUpdateIntervalForType, SensorTypes } from "react-native-sensors";

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height)
const aspectRatio = screenWidth / screenHeight;

class App extends PureComponent {

  constructor() {
    super()
    this.state = {
      barcodePositions: [],
    }
    setUpdateIntervalForType(SensorTypes.accelerometer, 400);
  }

  render() {

    const { barcodePositions } = this.state
    accelerometer.subscribe(this.handleAccelerometr.bind(this));

    return (
      <View style={styles.container}>
        {
          barcodePositions.map(({ x, y, width, height, data }) => {
            return (
              <>
                <View style={[styles.bounds, { top: y, left: x, width, height, borderColor: "tomato", borderWidth: 1 }]} />
                <Text style={[styles.boundsText, { top: y + height + 5, left: x, }]}>{data}</Text>
              </>
            )
          })
        }

        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          captureAudio={false}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          onGoogleVisionBarcodesDetected={({ barcodes }) => {
            this.setState({
              ...this.state,
              barcodePositions: barcodes
                .filter(({ type }) => type !== "UNKNOWN_FORMAT")
                .map(({ bounds, data }) => {
                  console.log(barcodes);
                  return {
                    x: bounds.origin.x,
                    y: bounds.origin.y,
                    width: bounds.size.width + (bounds.size.width * aspectRatio),
                    height: bounds.size.height,
                    data
                  }
                })
            })
          }}
        />
        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
            <Text style={{ fontSize: 14 }}> SNAP </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  takePicture = async () => {
    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      const data = await this.camera.takePictureAsync(options);
      console.log(data.uri);
    }
  };

  handleAccelerometr = ({ x, y, z, timestamp }) => {
    console.log({ x, y, z, timestamp })
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
    position: "relative",
    width: screenWidth,
    height: screenHeight
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },

  bounds: {
    zIndex: 100,
    position: "absolute",
  },

  boundsText: {
    zIndex: 100,
    color: "white",
    position: "absolute",
    backgroundColor: "black",
    opacity: 0.5
  },

  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
});


export default App;
